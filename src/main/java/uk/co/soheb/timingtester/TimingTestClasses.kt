package uk.co.soheb.timingtester

interface ITimingTest {
    fun runTimingTest()
}

data class TimingEntry(
        val delayInMilliseconds: Int,
        val runnable: Runnable
)

abstract class IterativeRunnable<T>(private val index: Int, private val it: T): Runnable {
    final override fun run() {
        runWithArgs(index, it)
    }

    abstract fun runWithArgs(index: Int, it: T)
}