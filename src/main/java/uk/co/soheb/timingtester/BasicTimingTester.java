package uk.co.soheb.timingtester;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class BasicTimingTester implements ITimingTest {

    private final Set<TimingEntry> entries;

    private final ScheduledExecutorService scheduler;

    private BasicTimingTester(int corePoolSize) {
        this.entries = new TreeSet<>(Comparator.comparingInt(TimingEntry::getDelayInMilliseconds));
        this.scheduler =  Executors.newScheduledThreadPool(corePoolSize);
    }

    public static BasicTimingTester basic() {
        return new BasicTimingTester(4);
    }

    public static BasicTimingTester customPoolSize(int corePoolSize) {
        return new BasicTimingTester(corePoolSize);
    }

    public void addEntry(int delayInMilliseconds, Runnable runnable) {
        this.entries.add(new TimingEntry(delayInMilliseconds, runnable));
    }

    protected List<Integer> getTimings() {
        return entries.stream().map(TimingEntry::getDelayInMilliseconds).collect(Collectors.toList());
    }

    @Override
    public void runTimingTest() {
        for (TimingEntry entry : entries) {
            scheduler.schedule(entry.getRunnable(), entry.getDelayInMilliseconds(), TimeUnit.MILLISECONDS);
        }
    }
}
