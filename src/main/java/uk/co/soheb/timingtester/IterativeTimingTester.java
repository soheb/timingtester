package uk.co.soheb.timingtester;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class IterativeTimingTester<T> implements ITimingTest {

    private final Map<Integer, T> entries;

    private final ScheduledExecutorService scheduler;

    private IterativeRunnable<T> iterativeRunnable;

    private IterativeTimingTester(int corePoolSize) {
        this.entries = new TreeMap<>();
        this.scheduler =  Executors.newScheduledThreadPool(corePoolSize);
    }

    public static <T> IterativeTimingTester<T> basic() {
        return new IterativeTimingTester<>(4);
    }

    public static <T> IterativeTimingTester<T> customPoolSize(int corePoolSize) {
        return new IterativeTimingTester<>(corePoolSize);
    }

    public void addEntry(int delayInMilliseconds, T object) {
        this.entries.put(delayInMilliseconds, object);
    }

    public void setIterativeRunnable(IterativeRunnable<T> it) {
        this.iterativeRunnable = it;
    }

    protected List<Integer> getTimings() {
        return new ArrayList<>(entries.keySet());
    }

    @Override
    public void runTimingTest() {
        for (Map.Entry<Integer, T> entry : entries.entrySet()) {
            scheduler.schedule(iterativeRunnable, entry.getKey(), TimeUnit.MILLISECONDS);
        }
    }
}
