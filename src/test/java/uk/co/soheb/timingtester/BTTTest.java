package uk.co.soheb.timingtester;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class BTTTest {

    private static final Runnable dummy = () -> { };

    @Test
    public void delayInOrder() {
        BasicTimingTester btt = BasicTimingTester.basic();
        btt.addEntry(15, dummy);
        btt.addEntry(5, dummy);
        btt.addEntry(10, dummy);

        List<Integer> entryList = btt.getTimings();
        Assert.assertEquals(3, entryList.size());
        Assert.assertEquals(Integer.valueOf(5), entryList.get(0));
        Assert.assertEquals(Integer.valueOf(10), entryList.get(1));
        Assert.assertEquals(Integer.valueOf(15), entryList.get(2));
    }
}
